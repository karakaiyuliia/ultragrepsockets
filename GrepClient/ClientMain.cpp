/*
file:		ClientMain.cpp
purpose:	contains all client functionality
author:		Yuliia Karakai, Abdullah Basaad
date:		05/12/2019
*/


#include "Socket.hpp"
#define _WINSOCK2API_
#define _WINSOCKAPI_   /* Prevent inclusion of winsock.h in windows.h */
#include "ConsoleLogger.hpp"
#include "../GrepServer/AppConstants.hpp"

#include <iostream>
#include <string>
#include <thread>
#include<vector>
#include<string>
#include <map>
#include <sstream>
#include <fstream>

using namespace  std;

#define cString(s)  string(s).c_str() 

CConsoleLogger another_console;
bool exitting = false;
bool verbose = false;
bool stop = false;
bool connected = true;
string ip_address = "127.0.0.1";
const unsigned short PORT = 49153;

//a struct to hold a list of all matches
struct match_t
{
	unsigned line;
	string match;

	match_t() : line(0) {}
	match_t(string match, unsigned line) :match(match), line(line) {}

};
typedef  std::vector<match_t> matches_list_t;
std::map<std::string, matches_list_t> matches;

void printMatchesReport() {
	int matchesCount = 0;

	another_console.print(cString("Matches Report: \n"));
	for (auto match : matches)
	{
		matchesCount += (int)match.second.size();

		another_console.print(string(match.first + " :" + "\n").c_str());
		for (auto matchElement : match.second)
			another_console.print(string(to_string(matchElement.line) + "| " + matchElement.match + "\n").c_str());
	}

	another_console.print(string("\nFiles with Matches: " + to_string(matches.size()) + "\n").c_str());
	another_console.print(string("Number of Matches: " + to_string(matchesCount) + "\n").c_str());

}

match_t getMatchFromString(string matchString, string& filename)
{
	stringstream ss(matchString);
	vector<string> result;
	match_t theMatch;
	for (size_t i = 0; ss.good(); i++)
	{
		string substr;
		getline(ss, substr, ';');
		if (substr == "match") continue;
		else if (i == 1) filename = substr;
		else if (i == 2) theMatch.line = stoi(substr);
		else if (i == 3) theMatch.match = substr;

	}

	return theMatch;


}

void PrintHelp()
{
	cout << "(drop): disconnects from the server." << endl;
	cout << "(connect [address]): connects the client the server at the specified address." << endl;
	cout << "(stopserver): instructs the grep server to shutdown." << endl;
	cout << "(grep [-v] folder expr [extention-list]): greps the server." << endl;
	cout << "(help): prints this list." << endl;

	cout << "\nEnter command:	";
}

void Print(const string& str)
{
	cout << str << endl;
	cout << "Enter command:	";
}
void receive(UDPSocket& client)
{

	while (!exitting)
	{
		string msg = client.receiveFromServer();

		if (msg == grepcommands::stopServerMsg)
			break;

		else if (msg == grepcommands::dropMsg)
			break;
		else if (msg == grepcommands::grepVerboseMsg)
			verbose = true;

		else if (msg == grepcommands::grepStartedMsg)
		{
			Print("Server is grepping..");

			if (verbose)
				another_console.print(string("Grep proccess has started in verbose mode, matches will be printed in this console.\n").c_str());
			else
				another_console.print(string("Grep proccess has started, report will be printed in this console when is done.\n").c_str());

		}

		else if (msg == grepcommands::grepFinishedMsg) {
			printMatchesReport();
			Print("Grep is done, report is printed in output console.");
			matches.clear();
			verbose = false;
			client.sendToServer(grepcommands::resetGrepMsg);


		}

		else if (msg.substr(0, 4) == "Grep")
		{

			Print(msg);
			continue;
		}
		else if (msg.size() >= 5)
		{

			if (msg.substr(0, 5) == "match")
			{
				string filename;
				match_t theMatch = getMatchFromString(msg, filename);

				if (verbose)
					another_console.print(string("v| match found: \"" + filename + "\"[" + to_string(theMatch.line) + "] (" + theMatch.match + ").\n").c_str());

				// storing the match into the map
				if (matches.find(filename) == matches.end()) {
					matches_list_t fileMatches = { theMatch };
					matches.insert(make_pair(filename, fileMatches));// add file scanned to matches map

				}
				else
					matches[filename].push_back(theMatch); // file already exists in the map,
														  //  just add the match to its vector.

			}

		}


	}



}

int main(int argc, char* argv[])
{

	another_console.Create("GrepClient Output console");
	another_console.print(cString("Client output console\n"));

	const string AppTitle = AppName + " V" + AppVersion + " By " + AppDevelopers + " " + AppVersionDate;
	cout << AppTitle << endl << endl;

	if (argc > 1)
		ip_address = argv[1];

	UDPSocket client(PORT);
	int res = client.createServer(false, ip_address);

	Print("Connected to: " + ip_address);

	string line;
	thread clientReceive(receive, ref(client));

	while (getline(cin, line)) {

		if (line == grepcommands::stopServerMsg)
		{
			if (connected)
			{

				client.sendToServer(line);
				stop = true;
				clientReceive.join();
				cout << "Server Stopped, quitting.." << endl;
				another_console.print(cString("Server Stopped, quitting.."));
				another_console.Close();
				return EXIT_SUCCESS;
			}


			else
				Print("Client is not connected.");
			
		}



		else if (line == grepcommands::dropMsg)
		{
			if (connected)
			{
				connected = false;
				Print("Client Connection Dropped.");
				client.sendToServer(grepcommands::dropMsg);
				clientReceive.join();
			}

			else
				Print("Client is not connected.");

			continue;
		}
		else if (line.size() >= 4)
		{

			if (line.substr(0, 4) == "grep")
			{
				if (connected)
					client.sendToServer(line);

				else
					Print("Client is not connected.");
				continue;
			}

			else if (line.substr(0, 4) == "help")
			{
				cout << "\nList of available commands: " << endl;
				PrintHelp();

				continue;
			}

			else if (line.size() >= 7)
				if (line.substr(0, 7) == "connect")
				{
					if (connected) {
						Print("Client is already connected To: " + ip_address);
						continue;
					}

					if (line.size() > 8)
					{
						string ip = line.substr(8, line.size() - 8);

						if (ip.size() >= 7)
							ip_address = ip;
					}
					else
						ip_address = "127.0.0.1";



					if (client.Connect(ip_address))
					{
						connected = true;
						Print("Connected to: " + ip_address);
						clientReceive = thread(receive, ref(client));
					}

					else
						Print("Error Connecting To: " + ip_address);

					continue;
				}
		}

		cout << "\n(" << line << ") is not a recognized command, here is a list of availble commands:" << endl;
		PrintHelp();


	}

	clientReceive.join();
}