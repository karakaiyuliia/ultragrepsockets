
#include "Socket.hpp"
#define _WINSOCK2API_
#define _WINSOCKAPI_   /* Prevent inclusion of winsock.h in windows.h */
#include "ConsoleLogger.hpp";


#include <iostream>
#include <string>


using namespace  std;
int main()
{
	UDPSocket client(49153);
	int res = client.createServer(false);


	CConsoleLogger another_console;
	another_console.Create("GrepClient Output console");

	string line;
	while (getline(cin, line)) {

		switch(line)

			case "stopserver": 

		client.sendToServer(line);
		string msg = client.receiveFromServer();

		if (msg == "")
			cout << "no reply" << endl;
		else {
			string const termianteMsg = "server exit";
			if (msg == termianteMsg) {
				cout << "Server terminated" << endl;
				break;
			}
			another_console.print(string(": " + msg).c_str());
			cout << ": " << msg << endl;
		}
	}



}