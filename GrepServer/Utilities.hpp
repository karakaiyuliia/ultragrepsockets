#pragma once

/*
file:		Utilities.hpp
purpose:	contains namespace with method for checking ability to convert string to regex
author:		Yuliia Karakai, Abdullah Basaad
date:		05/12/2019
*/

#include "regex"


namespace Utilities
{
	bool extStrToReg(std::string extList, std::regex& extentions);

}