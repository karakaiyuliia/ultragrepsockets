/*
file:		ServerMain.cpp
purpose:	server's main functionality
author:		Yuliia Karakai, Abdullah Basaad
date:		05/12/2019
*/

#include "Socket.hpp"
#include <iostream>
#include <string>
#include <thread>
#include <iostream> 
#include <regex> 
#include<filesystem>
#include <string>
#include "AppConstants.hpp"
#include "Utilities.hpp"
#include <map>
#include <fstream>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>
#include <chrono>
#include <sstream>

using namespace std;
using namespace std::filesystem;


// Application Properties
const string AppTitle = AppName + " V" + AppVersion + " By " + AppDevelopers + " " + AppVersionDate;
bool verbouseMode = false;
path folderPath = "";
regex expr;
regex extentions(".txt");
string ip_address = "127.0.0.1";
const unsigned short PORT = 49153;

mutex matchesMtx;

// Thread managament
unsigned nThreads = 4; // will get changed depennding on system cores available
bool morePossibleFiles = true;
bool terminateScan = false;
bool scaning = false;


// wake
mutex wakeMtx;
condition_variable wakeCond;

// Console lock
mutex consoleMtx;
void consolePrint(string str)
{
	lock_guard<mutex> lk(consoleMtx);
	cout << str << endl;
}



// queue of files to scan
queue<path> foundFiles;
mutex foundFilesMtx;


// barrier
unsigned barrierThreshold = nThreads + 1;
unsigned barrierCount = barrierThreshold;
unsigned barrierGeneration = 0;
mutex barrierMtx;
condition_variable barrierCond;
void inline setBarrierVariables()
{
	barrierThreshold = nThreads + 1;
	barrierCount = barrierThreshold;
}
void barrier() {
	unique_lock<mutex> lock(barrierMtx);
	unsigned gen = barrierGeneration;
	if (--barrierCount == 0) {
		++barrierGeneration;
		barrierCount = barrierThreshold;
		barrierCond.notify_all();

	}
	else {

		while (gen == barrierGeneration)
			barrierCond.wait(lock);
	}
}

// udp client
Socket::SocketAddress client;
UDPSocket* ServerSocket;


// scanning functions
void regexScan(path filePath) {

	// open file
	ifstream infile(filePath);
	std::string line;
	const string filename = filePath.filename().string();

	int lineNum = 1;
	// read file line by line
	while (std::getline(infile, line))
	{
		smatch match;

		// look for matches in the line, add to matches map if a match found.
		while (regex_search(line, match, expr)) {

			ServerSocket->send("match;" + filename + ";" + to_string(lineNum) + ";" + match.str(0), client);
			line = match.suffix().str();
		}
		lineNum++;
	}

	if (infile)
		infile.close();
}

//look for files in directory
void filesScan(path const& f, vector<path>& allFiles, regex reg, bool rec) {
	if (terminateScan)
		return;

	directory_iterator d(f);	// first entry of folder 'f'
	directory_iterator e;		// virtual match to the 'end' of any folder

	if (rec)
		while (d != e) {


			if (regex_match(d->path().extension().string(), reg))
			{
				{lock_guard<mutex> lk(foundFilesMtx);
				foundFiles.push(d->path()); }
				wakeCond.notify_one();

			}



			else if (is_directory(d->path()))
				filesScan(d->path(), allFiles, reg, rec);
			++d;
		}

	else
		while (d != e) {

			if (regex_match(d->path().extension().string(), reg))
			{
				{lock_guard<mutex> lk(foundFilesMtx);
				foundFiles.push(d->path()); }
				wakeCond.notify_one();
			}
			++d;
		}
}


// Scanning a file in a thread
void perform_scan() {

	barrier();

	while (morePossibleFiles) {

		{ unique_lock<mutex> lk(wakeMtx);
		wakeCond.wait(lk); }

		while (!foundFiles.empty()) {
			path filepath;
			bool haveTask = false;
			{ // DCLP - Double Check Lock Pattern
				lock_guard<mutex> lk(foundFilesMtx);
				if (!foundFiles.empty()) {
					filepath = foundFiles.front();
					foundFiles.pop();
					haveTask = true;
				}
			}

			if (haveTask) {

				regexScan(filepath);
			}
		}
	}


}

//convert string command and return error if it's incorrect
bool parseGrepCommand(string command)
{
	stringstream ssin(command);
	vector<string> cmds = {};
	while (ssin.good()) {
		string s;
		ssin >> s;
		cmds.push_back(s);

	}

	// Check if folder path and expression are actually given, exit if not
	if (cmds.size() < 3)
	{
		cout << "Grep: No sufficent arguments provided!\n" << endl;
		ServerSocket->send("Grep: No sufficent arguments provided!\n", client);
		return false;
	}

	else

	{
		unsigned processArg = 1;
		// if a switch is provided
		if (string(cmds[processArg]).size() == 2)
			if (cmds[processArg][0] == '-')
			{
				//  Switch on mode if it is known, ignore if not..
				if (regex_search((cmds[processArg]), regex("v|V"))) { verbouseMode = true; }
				else cout << "Grep: Ignoring unknown switch: " << cmds[processArg] << endl;

				processArg++;
			}

		if (is_directory(cmds[processArg]))
			folderPath = cmds[processArg++];
		else
		{
			cout << "Grep: Folder path: '" << cmds[processArg] << "' is not an acceptable path." << endl;
			ServerSocket->send(("Grep: Folder path: '" + cmds[processArg] + "' is not an acceptable path."), client);
			return false;
		}
		if (processArg < cmds.size())
			expr = regex(cmds[processArg++]);
		else
		{
			ServerSocket->send("Grep: Regex is not provided.", client);
			return false;
		}

		if (processArg < cmds.size())
			if (!Utilities::extStrToReg(cmds[processArg], ref(extentions)))
			{

				cout << "Grep: Error parsing extension list: Extensions must only contain English alphapet." << endl;
				ServerSocket->send("Grep: Error parsing extension list: Extensions must only contain English alphapet.", client);
				return false;
			}

		return true;

	}
}

//execute grep
void grepRun()
{
	// Getting # of cores available C++11 way. 

	nThreads = std::thread::hardware_concurrency() * 2; // multiplied to include logical cores
	setBarrierVariables();

	// launch worker threads.
	vector<thread> threads;
	for (unsigned i = 0; i < nThreads; ++i)
		threads.push_back(thread(perform_scan));

	barrier();

	vector<path> allFiles = {};

	cout << "Grep started. # of threads used: " << nThreads << endl;
	filesScan(folderPath, allFiles, extentions, true);
	morePossibleFiles = false;
	wakeCond.notify_all();


	// cleanup
	for (auto& t : threads)
		t.join();

	if (!terminateScan)
		ServerSocket->send(grepcommands::grepFinishedMsg, client);
}

void serverRun()
{
	thread grepThread;
	for (;;) {

		string msg = ServerSocket->receive(client);

		if (msg == grepcommands::stopServerMsg) {
			terminateScan = true;
			morePossibleFiles = false;
			if (scaning)
				grepThread.join();

			break;
		}
		else if (msg == grepcommands::resetGrepMsg)
		{
			grepThread.join();
			morePossibleFiles = true;
			scaning = false;
			extentions = regex(".txt");

		}
		else if (msg == grepcommands::dropMsg)
		{
			cout << "Client disconnected." << endl;
			ServerSocket->send(grepcommands::dropMsg, client);

			continue;
		}
		else if (msg.substr(0, 4) == "grep")
		{
			if (parseGrepCommand(msg))
			{
				if (verbouseMode)
					ServerSocket->send(grepcommands::grepVerboseMsg, client);

				ServerSocket->send(grepcommands::grepStartedMsg, client);
				scaning = true;
				grepThread = thread(grepRun);

			}

			continue;
		}

	}

}

int main(int argc, char* argv[])
{

	const string AppTitle = AppName + " V" + AppVersion + " By " + AppDevelopers + " " + AppVersionDate;
	cout << AppTitle << endl << endl;

	if (argc == 2)
		ip_address = argv[1];

	ServerSocket = new UDPSocket(PORT);
	int res = ServerSocket->createServer(true, ip_address);
	if (res != EXIT_SUCCESS) {
		cerr << "Error on bind\n";
		cout << "Result: " << res << endl;
		return EXIT_FAILURE;
	}
	cout << "UltraGrepSocket Server is bound to " << ip_address << endl;


	thread serverSending(serverRun);
	serverSending.join();

	cout << "UltraGrepSocket Server is shutdown\n";
	ServerSocket->send(grepcommands::stopServerMsg, client);
}