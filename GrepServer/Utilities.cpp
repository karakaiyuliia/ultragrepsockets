#include "Utilities.hpp"

using namespace std;
namespace Utilities {
	bool extStrToReg(string extList, regex& extentions)
	{
		string fixedExtList = "";
		string newExt = "";
		for (char c : extList)
		{
			if (c == '.')
			{
				fixedExtList += newExt + "|.";
				newExt = "";
			}
			else  if (!isalpha(c)) return false;
			else newExt += c;

		}
		fixedExtList += newExt;

		// cleaning up
		if (*fixedExtList.begin() == '|')     fixedExtList.erase(0, 1);
		if (*(fixedExtList.end() - 1) == '|') fixedExtList.erase(fixedExtList.size() - 1);

		extentions = regex(fixedExtList);


		return true;
	}

}