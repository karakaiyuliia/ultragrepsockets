#pragma once

/*
file:		AppConstants.hpp
purpose:	defines application constants for convenience
author:		Yuliia Karakai, Abdullah Basaad
date:		05/12/2019
*/

#include <iostream> 

const std::string AppDevelopers = "Abdullah Basaad, Yuliia Karakai";
const std::string AppVersion = "1.2";
const std::string AppVersionDate = "2019";
const std::string AppName = "UltraGrepScoket";

namespace grepcommands {
const std::string resetGrepMsg = "reset grep";
const std::string stopServerMsg = "stopserver";
const std::string grepVerboseMsg = "grep verbose";
const std::string grepStartedMsg = "grep started";
const std::string grepFinishedMsg = "grep done";
const std::string dropMsg = "drop";


}
