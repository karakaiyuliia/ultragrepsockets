#include "Socket.hpp"
#include<iostream>

int UDPSocket::initializeWsa() {
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);	//load socket dll and tell me if it support socket interface 2.2
	if (iResult != 0) {
		cerr << "WSAStartup failed: " << iResult << endl;
		return EXIT_FAILURE;
	}
}

int UDPSocket::createServer(const unsigned short port) {
	
	sockaddr_in serverAddress = { 0 };
	serverAddress.sin_family = AF_INET;

	const unsigned short PORT = port;
	serverAddress.sin_port = htons(PORT);
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);

	int res = bind(hSocket, (sockaddr*)&serverAddress, sizeof(sockaddr_in));

	if (res == SOCKET_ERROR) {
		res = WSAGetLastError();
		cleanup();
		return res;
	}
}

string UDPSocket::receive(sockaddr* from, int* fromlen, char msg[]) {
	int n = recvfrom(hSocket, msg, sizeof(msg)+1, 0, from, fromlen);
	return "NULL";
}

void UDPSocket::send(sockaddr* tp, string msg) {
	sendto(hSocket, msg.c_str(), msg.size(), 0, tp, sizeof(tp));
}

void UDPSocket::cleanup() {
	if(hSocket != null;)
	closesocket(hSocket);
	WSACleanup();
}