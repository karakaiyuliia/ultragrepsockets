#pragma once

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <string>
using namespace std;

#pragma comment (lib, "ws2_32.lib") 
class Socket {
protected: Socket(SOCKET s): hSocket(s) {}
		WSAData wsaData;
		SOCKET hSocket;

};
class UDPSocket: Socket {



public:
	UDPSocket():Socket(socket(AF_INET, SOCK_DGRAM, 0)) { };
	int initializeWsa();
	int createServer(const unsigned short port);
	string receive(sockaddr* from, int* fromlen, char msg[]);
	void send(sockaddr* tp, string msg);
	~UDPSocket() { cleanup(); }

	private:
	void cleanup();

};