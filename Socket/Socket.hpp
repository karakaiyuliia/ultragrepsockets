#pragma once

/*
file:		Socket.hpp
purpose:	socket facade class declaration
author:		Yuliia Karakai, Abdullah Basaad
date:		05/12/2019
*/

#include <WinSock2.h>

#include <string>



class Socket {
private: 
	const WORD socketInterfaceV = MAKEWORD(2, 2); //socket interface 2.2
	int initializeWsa();
protected: Socket(SOCKET s, const unsigned short port) : hSocket(s), port(port){ initializeWsa(); }
		   WSAData wsaData;
		   SOCKET hSocket;
		   unsigned short port;
public:
		typedef sockaddr SocketAddress;

		

};
class UDPSocket : Socket {

public:
	UDPSocket(const unsigned short port) :Socket(socket(AF_INET, SOCK_DGRAM, 0), port), serverAddress({}) { };
	 int createServer(bool isBound, std::string address = "127.0.0.1" );
	std::string receive(SocketAddress& from );
	void send(std::string msg,SocketAddress& tp);
	std::string receiveFromServer();
	void sendToServer(std::string msg);
	~UDPSocket() { cleanup(); }
	bool Connect(std::string address = "127.0.0.1");

private:
	void cleanup();
	sockaddr_in serverAddress;
	
};