/*
file:		Socket.cpp
purpose:	socket facade class implementation
author:		Yuliia Karakai, Abdullah Basaad
date:		05/12/2019
*/

#include "Socket.hpp"
#include <WS2tcpip.h>
#include<iostream>
#pragma comment (lib, "ws2_32.lib") 

#pragma region Socket implementations
int Socket::initializeWsa() {
	int iResult = WSAStartup(socketInterfaceV, &wsaData);	//load socket dll and tell me if it support socket interface.
	if (iResult != 0) {
		std::cerr << "WSAStartup failed: " << iResult << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
#pragma endregion



#pragma region UDPSocket implementations
int UDPSocket::createServer(bool isBound, std::string address) {
	hSocket = socket(AF_INET, SOCK_DGRAM, 0);

	serverAddress = { 0 };
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(port);
	inet_pton(AF_INET, address.c_str(), &(serverAddress.sin_addr));

	if (isBound) {
		int res = bind(hSocket, reinterpret_cast<sockaddr*>(&serverAddress), sizeof(sockaddr_in));
		if (res == SOCKET_ERROR) {

			res = WSAGetLastError();
			closesocket(hSocket);
			return res;
		}

	}




	return EXIT_SUCCESS;

}

std::string UDPSocket::receiveFromServer()
{
	int const MAXLINE = 256;
	char msg[MAXLINE];
	int nChars;

	nChars = recvfrom(hSocket, msg, MAXLINE, 0, NULL, NULL);

	if (nChars == -1)
		return "";
	else
	{
		std::string s = "";
		for (int i = 0; i < nChars; i++)
		{
			s += msg[i];
		}

		return s;
	}

}
std::string UDPSocket::receive(SocketAddress& from) {
	int const MAXLINE = 256;
	char msg[MAXLINE];
	int nChars;

	int cbClientAddress = sizeof(from);
	nChars = recvfrom(hSocket, msg, MAXLINE, 0, &from, &cbClientAddress);


	if (nChars == -1)
		return "";
	else
	{
		std::string s = "";
		for (int i = 0; i < nChars; i++)
		{
			s += msg[i];
		}

		return s;
	}


}

void UDPSocket::send(std::string msg, SocketAddress& tp) {
	sendto(hSocket, msg.c_str(), (int)msg.size(), 0, &tp, (int)sizeof(tp));
}
void UDPSocket::sendToServer(std::string msg)
{
	sendto(hSocket, msg.c_str(), (int)msg.size(), 0, reinterpret_cast<sockaddr*>(&serverAddress), (int)sizeof(serverAddress));
}

bool UDPSocket::Connect(std::string address)
{
	if (createServer(false, address) == EXIT_SUCCESS)
		return true;
	else
		return false;


}

void UDPSocket::cleanup() {
	closesocket(hSocket);
	WSACleanup();
}

#pragma endregion

